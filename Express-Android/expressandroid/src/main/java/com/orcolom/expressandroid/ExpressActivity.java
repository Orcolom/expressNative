package com.orcolom.expressandroid;

import com.unity3d.player.UnityPlayerActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

public class ExpressActivity extends UnityPlayerActivity {

    protected void onCreate(Bundle savedInstanceState) {
        // call UnityPlayerActivity.onCreate()
        super.onCreate(savedInstanceState);
        // print debug message to logcat
        Log.d("ExpressActivity", "onCreate called!");

        View view = getWindow().getDecorView().findViewById(android.R.id.content);
//        view.setOnHoverListener(new View.OnHoverListener() {
//            @Override
//            public boolean onHover(View v, MotionEvent event) {
//                Log.d("ExpressActivity", event.toString());
//                return  true;
//            }
//        });
        view.setAccessibilityDelegate(new View.AccessibilityDelegate(){
            @Override
            public boolean onRequestSendAccessibilityEvent(ViewGroup host, View child, AccessibilityEvent event) {
                Log.d("ExpressActivity", event.toString());
                return super.onRequestSendAccessibilityEvent(host, child, event);
            }

        });
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        Log.d("ExpressActivity", event.toString());
        return  false;
    }
}
