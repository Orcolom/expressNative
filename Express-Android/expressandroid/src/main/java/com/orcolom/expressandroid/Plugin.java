package com.orcolom.expressandroid;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import com.unity3d.player.UnityPlayer;

public class Plugin {

    static TextToSpeech _tts;
    static float _volume = 1.f;
    static float _rate = 1.f;

    public static String getTextFromPlugin(int nmr) {
        return  "The number is " + nmr;
    }

    public static int initExpress()
    {
        InitListener listener = new InitListener();
        _tts = new TextToSpeech(UnityPlayer.currentActivity, listener);

        do {}
        while (!listener.isInitialized);
        return listener.result;
    }

    public static int speak(String str, boolean intercept)
    {
        if (intercept)
            _tts.stop();

        Bundle b = new Bundle();
        b.putFloat(TextToSpeech.Engine.KEY_PARAM_VOLUME, _volume);

        return _tts.speak(str, 0, b, null);
    }

    public static void silence()
    {
        _tts.stop();
    }


    public static void destroyExpress()
    {
        _tts.shutdown();
    }

    public static void setVolume(int volume)
    {
        _volume = volume / 100.0f;
    }

    public static int getVolume()
    {
        return (int)(_volume * 100.0f);
    }

    public static void setRate(int rate)
    {
        _rate = rate;
        if (_rate >= 0)
            _rate += 1;
        if (_rate < 0)
            _rate = 1 - (rate / -10.0f);
        _tts.setSpeechRate(_rate);
    }

    public static int getRate()
    {
        return  (int)_rate;
    }
}

class InitListener implements TextToSpeech.OnInitListener {

    public boolean isInitialized;
    public int result;

    @Override
    public void onInit(int i) {
        isInitialized = true;
        result = i;
    }
}