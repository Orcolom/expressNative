// Express-Win-Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../Express-Win/Express.h"
#include <iostream>
// #define EXPRESS_IMPORT

int main()
{
  Express::initExpress();
  Express::speak("hey, this is a sentance", false);
  Express::speak("some more sentances", false);
  Express::speak("SENTANCES!!!!", false);
  Express::speak("override", true);
  std::cin.get();

  auto vol = Express::getVolume();
  Express::speak("Loud", false);
  std::cin.get();
  
  Express::setVolume(10);
  Express::speak("silent", false);
  std::cin.get();
  
  Express::setVolume(100);

  auto rate = Express::getRate();
  Express::speak("normal", false);
  std::cin.get();
  
  Express::setRate(10);
  Express::speak("fast", false);
  std::cin.get();
  
  Express::setRate(-10);
  Express::speak("slow", false);
  std::cin.get();
  Express::setRate(0);

  Express::speak("pres enter to stop this string brefore it ends, this is a test fro wether or not silence works, please click now before it ends. the end...", false);
  std::cin.get();

  Express::silence();  
  std::cin.get();

  Express::destroyExpress();
}

