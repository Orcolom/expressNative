// Express-Win.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Express.h"
#include "Readers/SapiScreenReader.h"

namespace Express
{
  long initExpress()
  {
    m_Reader = new SapiScreenReader();

    auto hr = NULL;

#ifdef EXPRESS_IMPORT
    hr = CoInitializeEx(NULL, COINITBASE_MULTITHREADED);
    if (FAILED(hr))
    return hr;

#endif // EXPRESS_IMPORT

    hr = m_Reader->initReader();

    //hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&_voice);
    if (FAILED(hr))
      return hr;
    
    return hr;
  }

  void destroyExpress()
  {
    m_Reader->destroyReader();
    delete m_Reader;
    // _voice->Pause();
    // _voice->Release();

#ifdef EXPRESS_IMPORT
    CoUninitialize();
#endif // EXPRESS_IMPORT
  }
  
  long speak(const char* text, bool interept)
  {
    return m_Reader->speak(text, interept);
    /*if (!_voice)
      return 1;

    int len = strlen(text) + 1;
    wchar_t *wText = new wchar_t[len];
    memset(wText, 0, len);
    ::MultiByteToWideChar(CP_ACP, NULL, text, -1, wText, len);

    DWORD flags = SPF_ASYNC| SPF_IS_NOT_XML;
    if (interept)
      flags |= SPF_PURGEBEFORESPEAK;
   
    return _voice->Speak(wText, flags, NULL);*/
  }

  void silence()
  {
    if (m_Reader)
      m_Reader->silence();
  }

  long getVolume()
  {
    if (!m_Reader)
      return 0;

    return m_Reader->getVolume();
  }

  void setVolume(long value)
  {
    if (m_Reader)
      m_Reader->setVolume(value);
  }

  long getRate()
  {
    if (!m_Reader)
      return 0;

    return m_Reader->getRate();
  }

  void setRate(long value)
  {
    if (m_Reader)
      m_Reader->setRate(value);
  }
}