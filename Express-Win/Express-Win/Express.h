#include "stdafx.h"

// #define EXPRESS_IMPORT

#ifdef EXPRESS_IMPORT
#define DLL_API __declspec(dllimport)
#else
#define DLL_API __declspec(dllexport)
#endif

#include "sapi.h"
#include "Readers/ScreenReaderBase.h"

namespace Express
{
#ifndef EXPRESS_IMPORT
  extern "C"
  {
#endif // _EXPORT
    DLL_API long _cdecl initExpress();
    DLL_API void _cdecl destroyExpress();

    DLL_API long _cdecl speak(const char* text, bool interept);
    DLL_API void _cdecl silence();

    DLL_API long _cdecl getVolume();
    DLL_API void _cdecl setVolume(long value);
    
    DLL_API long _cdecl getRate();
    DLL_API void _cdecl setRate(long value);
#ifndef EXPRESS_IMPORT
}
#endif


  ScreenReaderBase* m_Reader;
}