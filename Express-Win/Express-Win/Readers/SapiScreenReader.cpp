#include "stdafx.h"
#include "SapiScreenReader.h"
#include "sapi.h"

SapiScreenReader::SapiScreenReader():
  m_spVoice(nullptr)
{
}


SapiScreenReader::~SapiScreenReader()
{
}

long SapiScreenReader::initReader()
{
  auto hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&m_spVoice);
  if (FAILED(hr))
    return hr;

  m_isInitialized = true;
  return hr;
}

void SapiScreenReader::destroyReader()
{
  m_spVoice->Pause();
  m_spVoice->Release();
}

long SapiScreenReader::speak(const char * str, bool interept)
{
  if (!m_spVoice)
    return 1;

  int len = (int)strlen(str) + 1;
  wchar_t *wText = new wchar_t[len];
  memset(wText, 0, len);
  ::MultiByteToWideChar(CP_ACP, NULL, str, -1, wText, len);

  DWORD flags = SPF_ASYNC | SPF_IS_NOT_XML;
  if (interept)
    flags |= SPF_PURGEBEFORESPEAK;

  return m_spVoice->Speak(wText, flags, NULL);
}

void SapiScreenReader::silence()
{
  m_spVoice->Pause();
}

void SapiScreenReader::setVolume(long vol)
{
  m_spVoice->SetVolume(vol);
}

long SapiScreenReader::getVolume()
{
  USHORT value = 0;
  auto hr = m_spVoice->GetVolume(&value);
  return value;
}

void SapiScreenReader::setRate(long rate)
{
  m_spVoice->SetRate(rate);
}

long SapiScreenReader::getRate()
{
  long value = 0;
  auto hr = m_spVoice->GetRate(&value);
  return value;
}
