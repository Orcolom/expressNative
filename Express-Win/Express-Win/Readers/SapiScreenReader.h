#pragma once

#include "ScreenReaderBase.h"
#include "sapi.h"

class SapiScreenReader : public ScreenReaderBase
{
public:
  SapiScreenReader();
  ~SapiScreenReader();

  long initReader();
  void destroyReader();

  long speak(const char* str, bool interept);
  void silence();

  void setVolume(long vol);
  long getVolume();

  void setRate(long vol);
  long getRate();

private:
  ISpVoice* m_spVoice;
};

