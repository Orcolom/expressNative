#pragma once

class ScreenReaderBase
{
public:
  ScreenReaderBase() {}
  ~ScreenReaderBase() {}

  virtual long initReader() = 0;
  virtual void destroyReader() = 0;
  
  virtual long speak(const char* str, bool interept) = 0;
  virtual void silence() = 0;


  virtual void setVolume(long vol) = 0;
  virtual long getVolume() = 0;

  virtual void setRate(long rate) = 0;
  virtual long getRate() = 0;

  bool IsInitialized() { return m_isInitialized; }

protected:
  bool m_isInitialized = false;
};
